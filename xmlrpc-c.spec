Name:		xmlrpc-c
Version:	1.51.06
Release:        1	
Summary:	Library implementing XML-based Remote Procedure Calls
License:	BSD and MIT
URL:		http://xmlrpc-c.sourceforge.net/
Source0:        https://sourceforge.net/projects/xmlrpc-c/files/Xmlrpc-c%20Super%20Stable/%version/xmlrpc-c-%version.tgz	

Patch0001:      0001-xmlrpc_server_abyss-use-va_args-properly.patch
Patch0003:      0001-add-meson-buildsystem-definitions.patch
Patch0004:      0002-chmod-x-xml-rpc-api2txt.patch

BuildRequires:	git-core meson >= 0.36.0 gcc gcc-c++ ncurses-devel 
BuildRequires:  libcurl-devel readline-devel pkgconfig(openssl)

Provides:       xmlrpc-c-c++ = %{version}-%{release}      xmlrpc-c-c++%{?_isa} = %{version}-%{release}
Obsoletes:      xmlrpc-c-c++ < %{version}-%{release}      

Provides:       xmlrpc-c-client = %{version}-%{release}   xmlrpc-c-client%{?_isa} = %{version}-%{release} 
Obsoletes:      xmlrpc-c-client < %{version}-%{release}    

Provides:       xmlrpc-c-client++ = %{version}-%{release} xmlrpc-c-client++%{?_isa} = %{version}-%{release}
Obsoletes:      xmlrpc-c-client++ < %{version}-%{release} 

Provides:       xmlrpc-c-apps = %{version}-%{release}     xmlrpc-c-apps%{?_isa} = %{version}-%{release}
Obsoletes:      xmlrpc-c-apps < %{version}-%{release}     

Provides:       bundled(expat)

%description
XML-RPC is a quick-and-easy way to make procedure calls 
over the Internet. It converts the procedure call into 
an XML document, sends it to a remote server using HTTP, 
and gets back the response as XML.

%package        devel
Summary:        Header files for xmlrpc-c
Requires:       %{name} = %{version}-%{release}

%description    devel
Header files for xmlrpc-c.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build

%install
%meson_install
%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license doc/COPYING lib/abyss/license.txt
%{_bindir}/xml*
%{_libdir}/*.so.*
%exclude %{_bindir}/xmlrpc-c-config

%files          devel
%defattr(-,root,root)
%{_bindir}/xmlrpc-c-config
%{_libdir}/libxmlrpc*.so
%{_libdir}/pkgconfig/xmlrpc*.pc
%{_includedir}/*.h
%{_includedir}/xmlrpc-c/

%files          help
%defattr(-,root,root)
%doc doc/CREDITS doc/HISTORY doc/TODO README 
%doc tools/xmlrpc_transport/xmlrpc_transport.html
%{_mandir}/man1/*

%changelog
* Tue Jul 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.51.06-1
- Type:NA
- Id:NA
- SUG:NA
- DESC: update to 1.51.06

* Tue Mar 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.51.03-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC: chmod xml-rpc-api2txt

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.51.03-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:delete the isa in obsoletes

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.51.03-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.51.03-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 1.51.03-1

* Sat Oct 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.51.0-7
- Package init
